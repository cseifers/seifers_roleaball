﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player_Controller : MonoBehaviour {

    // These are defining the variables we use later such as winText

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    Renderer rend;
    private int count;
    private float movement;

    void Start ()
    {

        // This is settimg certain componets such as count to 0
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        rend = GetComponent<Renderer>();
        winText.text = "";
    }

    void FixedUpdate()
    {
        // This is setting up the movement of the player both vertically and horizontally. 

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce (movement * speed);
    }
    void OnTriggerEnter(Collider other)
    {
        // this makes the counter go uo by 1 for every pick up gathered

        if (other.gameObject.CompareTag("Pick up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();
        }
        // makes the ball go 1 faster every time it picks up a pick up
        speed += 1f;

        // makes the color of the ball change to the color of the pick up

        rend.material = other.gameObject.GetComponent<Renderer>().material;

    }
    void SetCountText ()
    {
        // makes the text by the ball set to "You Win!" if the count text from earlier is equal or greater than 12

        countText.text = "Count: " + count.ToString ();
        if (count >= 12)
        {
            winText.text = "You Win!";
        }
    }
}
