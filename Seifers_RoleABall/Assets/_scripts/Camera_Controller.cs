﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour
{
    public GameObject player;

    Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {

        // this is the adjustment that will be done to the camera later

        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // this is the update done to the camera to stay with the player

        transform.position = player.transform.position + offset;
    }
}
